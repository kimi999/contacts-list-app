import React from 'react'

import { HashRouter, Route, Switch, NavLink } from 'react-router-dom'

import authService from './auth-service'
import Login from './login'
import ContactListing from './contacts/contact-listing'
import ContactEdit from './contacts/contact-edit'
import ContactDetail from './contacts/contact-detail'
import ContactNew from './contacts/contact-new'
import UserListing from './users/user-listing'
import UserEdit from './users/user-edit'
import UserDetail from './users/user-detail'
import UserNew from './users/user-new'

import ROUTES from './routes'

const App = () =>
  <HashRouter>
    <div>
      <Navigation brand="ContactsApp" />
      {(!authService.isLoggedIn()) && <Login />}
      {(authService.isLoggedIn()) && <div>
        <Switch>
          <Route path={ROUTES.USER_NEW} component={UserNew} />
          <Route path={ROUTES.USER_EDIT} component={UserEdit} />
          <Route path={ROUTES.USER_DETAIL} component={UserDetail} />
          <Route path={ROUTES.USER_LISTING} component={UserListing} />
          <Route path={ROUTES.CONTACT_NEW} component={ContactNew} />
          <Route path={ROUTES.CONTACT_EDIT} component={ContactEdit} />
          <Route path={ROUTES.CONTACT_DETAIL} component={ContactDetail} />
          <Route path={ROUTES.CONTACT_LISTING} component={ContactListing} />
        </Switch>
      </div>}
    </div>
  </HashRouter>

const Navigation = (props) =>
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <a className="navbar-brand">{props.brand}</a>
    {(authService.isLoggedIn()) && <div className="d-fles justify-content-between">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <NavLink exact className="nav-link" to={ROUTES.CONTACT_LISTING}>Contacts</NavLink>
        </li>
        <li className="nav-item">
          <NavLink exact className="nav-link" to={ROUTES.USER_LISTING}>Users</NavLink>
        </li>
        <li>
          <a className="btn btn-danger" onClick={() => authService.logout()}>Logout</a>
        </li>
      </ul>
    </div>}
  </nav>

export default App;