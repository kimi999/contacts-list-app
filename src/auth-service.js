let loggedIn = false

export default {
    isLoggedIn() {
        return loggedIn
    },

    async init() {

    },

    login() {
        loggedIn = true
    },

    logout() {
        loggedIn = false
    }

}

//import axios from 'axios'
//
//export default {
//  user: null,
//
//  onChange: null,
//
//  async reload() {
//    try {
//      const res = await axios.get('http://localhost:3000/user')
//     this.user = res.data
// }catch (e){
//    this.user = null
//        }
//       this.onChange()
//   },
//
//    isLoggedIn() {
//      return this.user !== null
// },
//
// async login() {
//   await axios.post('http://localhost:3000/login')
//      this.reload()
// },
//
// async logout() {
//   await axios.post('http://localhost:3000/logout')
// this.reload()
//    }
//}