import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'
import contactsService from './contacts-service'
import Collapsible from '../collapsible'

class ContactDetail extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            contact: {}
        }
    }

    componentWillMount() {
        this.load(this.props.match.params.id)
    }

    componentWillReceiveProps(newProps) {
        if (this.props.match.params.id !== newProps.match.params.id) {
            this.load(newProps.match.params.id)
        }
    }

    async deleteThisContact() {
       await contactsService.deleteContact(this.state.contact.id)
       this.props.history.push(ROUTES.CONTACT_LISTING)
    }

    async load(id) {
        const res = await contactsService.getContact(id)

        this.setState(
            { contact: res.data }
        )
    }

    render() {
        const contact = this.state.contact

        return (
            <div>
                <h2>Detail</h2>
                <div className="btn-group">
                    <Link className="btn btn-light" to={ROUTES.getUrl(ROUTES.CONTACT_EDIT, { id: contact.id })}>Edit</Link>
                    <button className="btn btn-danger" onClick={() => this.deleteThisContact()}>Delete</button>
                </div>

                <table>
                    <tbody>
                        <tr key={contact.id}>
                            <th>Name</th>
                            <td>{contact.name}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>{contact.phone}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{contact.email}</td>
                        </tr>
                        <tr>
                            <th>City</th>
                            <td>{contact.city}</td>
                        </tr>
                        <tr>
                            <th>Note</th>
                            <td>
                                <Collapsible>
                                    {contact.note}
                                </Collapsible>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ContactDetail