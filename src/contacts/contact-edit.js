import React from 'react'
import ROUTES from '../routes'
import contactsService from './contacts-service'
import ContactForm from './contact-form'

class ContactEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contact: null
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
  }

  async load(id) {
    const res = await contactsService.getContact(id)
    this.setState(
      { contact: res.data }
    )
  }

  async update() {
    await contactsService.updateContact(this.state.contact)
    this.props.history.push(ROUTES.CONTACT_LISTING)
  }


  render() {
    const contact = this.state.contact

    return (
      <div>
        {contact && <ContactForm contact={contact} />}
        <button className="btn btn-primary" onClick={() => this.update()}>Save contact</button>
      </div>
    )
  }
}

export default ContactEdit