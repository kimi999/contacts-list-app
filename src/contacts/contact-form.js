import React from 'react'

const ContactForm = ({ contact }) => {
    const handleChange = (e) => {
        contact[e.target.name] = e.target.value
    }

    return (
        <form>
            <div className="form-row">
                <div className="form-group container-fluid">
                    <label>Name:</label>
                    <input type="text" className="form-control" name="name" value={contact.name} onChange={handleChange} />
                </div>
                <div className="form-group container-fluid">
                    <label>E-mail:</label>
                    <input type="text" className="form-control" name="email" value={contact.email} onChange={handleChange} />
                </div>
                <div className="form-group container-fluid">
                    <label>Phone:</label>
                    <input type="text" className="form-control" name="phone" value={contact.phone} onChange={handleChange} />
                </div>
                <div className="form-group container-fluid">
                    <label>City:</label>
                    <input type="text" className="form-control" name="city" value={contact.city} onChange={handleChange} />
                </div>
                <div className="form-group container-fluid">
                    <label>Note:</label>
                    <textarea className="form-control" name="note" value={contact.note} onChange={handleChange} rows="5">
                    </textarea>
                </div>
            </div>
        </form>
    )
}

export default ContactForm