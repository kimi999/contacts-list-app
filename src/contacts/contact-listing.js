import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'
import contactsService from './contacts-service'
import { Icon } from 'react-fa'
import './contact-search-style.css';

class ContactListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      search: null,
      contacts: []
    }

    this.searchChange = this.searchChange.bind(this)
  }

  searchChange({ target }) {
    this.setState({ [target.name]: target.value })
    this.search(target.value)
  }

  componentWillMount() {
    this.search()
    this.load()
  }

  async search(search) {
    const sear = await contactsService.searchContacts(search)

    this.setState(
      { contacts: sear.data }
    )
  }

  async load() {
    const res = await contactsService.getContacts()

    this.setState(
      { contacts: res.data }
    )
  }

  render() {
    const contacts = this.state.contacts

    return (
      <div>
        <div className="row">
          <div className="col-md-3 list-group-item list-group-item-action active">Contacts</div>
          <div className="col-md-3 input-group stylish-input-group"><input placeholder="Search..." type="text" className="form-control" value={this.state.search} onChange={this.searchChange} />
            <span class="input-group-addon">
              <Icon name="search" />
            </span>
          </div>
          {contacts.map(c => (
            <Link to={ROUTES.getUrl(ROUTES.CONTACT_DETAIL, { id: c.id })} key={c.id} className="list-group-item list-group-item-action">{c.name}</Link>
          ))}
        </div>
        <Link to={ROUTES.CONTACT_NEW} className="btn btn-light" >Create new</Link>
      </div>
    )
  }
}

export default ContactListing