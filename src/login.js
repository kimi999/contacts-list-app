import React from 'react'
import authService from './auth-service'

const Login = () =>
    <div className="container-fluid">

        <form>
            <h2>Please sign in</h2>
            <label className="sr-only">Email address</label>
            <input type="email" id="inputEmail" className="form-control" placeholder="Email address" />
            <label className="sr-only">Password</label>
            <input type="password" id="inputPassword" className="form-control" placeholder="Password" />
            <div className="checkbox">
                <label>
                    <input type="checkbox" value="remember-me" name="rememberMe"/> 
                    Remember me!
                </label>
            </div>
            <button className="btn btn-lg btn-primary" onClick={() => authService.login()}>Sign in</button>
        </form>

    </div>

export default Login
