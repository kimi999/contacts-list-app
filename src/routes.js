export default {
  CONTACT_LISTING: '/contacts/',
  CONTACT_NEW: '/contact/new',  
  CONTACT_EDIT: '/contact/edit/:id',
  CONTACT_DETAIL: '/contact/detail/:id',
  CONTACT_SEARCH: '/contacts?q=:search',

  USER_LISTING: '/users/',
  USER_NEW: '/user/new/',
  USER_EDIT: '/user/edit/:id',
  USER_DETAIL: '/user/detail/:id',

  getUrl(path, params) {
    return path.replace(/:(\w+)/g, (m, k) => params[k])
  }
}