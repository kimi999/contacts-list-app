import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'
import usersService from './users-service'

class UsersDetail extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            users: {}
        }
    }

    componentWillMount() {
        this.load(this.props.match.params.id)
    }

    componentWillReceiveProps(newProps) {
        if (this.props.match.params.id !== newProps.match.params.id) {
            this.load(newProps.match.params.id)
        }
    }

    async load(id) {
        const res = await usersService.getUser(id)

        this.setState(
            { user: res.data }
        )
    }

    render() {
        const user = this.state.user

        return (
            <div>
                <h2>User detail</h2>
                <div className="btn-group">
                    <Link className="btn btn-light" to={ROUTES.getUrl(ROUTES.USER_EDIT, { id: user.id })}>Edit</Link>
                    <Link className="btn btn-danger" to={ROUTES.USER_LISTING}>Delete</Link>
                </div>

                <table>
                    <tbody>
                        <tr key={user.id}>
                            <th>Name</th>
                            <td className="table-dark">{user.name}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td className="table-primary">{user.phone}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td className="table-secondary">{user.email}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default UsersDetail