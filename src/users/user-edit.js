import React from 'react'
import ROUTES from '../routes'
import usersService from './users-service'
import UserForm from './user-form'

class UserEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user: null
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
  }

  async load(id) {
    const res = await usersService.getUser(id)
    this.setState(
      { user: res.data }
    )
  }

  async update() {
    await usersService.updateUser(this.state.user)
    this.props.history.push(ROUTES.USER_LISTING)
  }

  async deleteThisUser() {
    await usersService.deleteUser(this.state.user.id)
    this.props.history.push(ROUTES.USER_LISTING)
}

  render() {
    const user = this.state.user

    return (
      <div>
        {user && <UserForm user={user} />}
        <button className="btn btn-primary" onClick={() => this.update()}>Save user</button>
        <button className="btn btn-danger" onClick={() => this.deleteThisUser()}>Delete user</button>
      </div>
    )
  }
}

export default UserEdit