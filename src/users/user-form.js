import React from 'react'

const UserForm = ({ user }) => {
    const handleChange = (e) => {
        user[e.target.name] = e.target.value
    }

    return (
        <form>
            <div className="form-row">
                <div className="form-group container-fluid">
                    <label>ID:</label>
                    <input type="text" className="form-control" value={user.id} readOnly />
                </div>
                <div className="form-group container-fluid">
                    <label>Login</label>
                    <input type="text" className="form-control" name="login" value={user.login} onChange={handleChange} />
                </div>
                <div className="form-group container-fluid">
                    <label>Name:</label>
                    <input type="text" className="form-control" name="name" value={user.name} onChange={handleChange} />
                </div>
                <div className="form-group container-fluid">
                    <label>E-mail:</label>
                    <input type="text" className="form-control" name="email" value={user.email} onChange={handleChange} />
                </div>
                <div className="form-group container-fluid">
                    <label>Phone:</label>
                    <input type="text" className="form-control" name="phone" value={user.phone} onChange={handleChange} />
                </div>
            </div>
        </form>
    )
}
export default UserForm