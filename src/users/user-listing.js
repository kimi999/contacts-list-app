import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import usersService from './users-service'

class UserListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      users: []
    }
  }
  componentWillMount() {
    this.load()
  }

  async load() {
    const res = await usersService.getUsers()

    this.setState(
      { users: res.data }
    )
  }

  render() {
    const users = this.state.users

    return (
      <div>
        <div className="list-group">
          <div className="col-3 list-group-item list-group-item-action active">Users</div>

          {users.map(u =>
            <Link to={ROUTES.getUrl(ROUTES.USER_EDIT, { id: u.id })} key={u.id} className="list-group-item list-group-item-action">{u.name}</Link>
          )}

        </div>
        <Link to={ROUTES.USER_NEW} className="btn btn-light" >Create new</Link>
      </div>
    )
  }
}

export default UserListing