import React from 'react'
import ROUTES from '../routes'

import userService from './users-service'

import UserForm from './user-form'

class UserNew extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            user: {
                login: "",
                name: "",
                email: "",
                phone: "",
            }
        }
    }

    create() {
        userService.createUser(this.state.user).then(res => {
            this.props.history.push(ROUTES.USER_LISTING)
        })
    }


    render() {
        const user = this.state.user

        return (
            <div>
                {user && <UserForm user={user} />}
                <button className="btn btn-primary" onClick={() => this.create()}>Create</button>
            </div>
        )
    }
}
export default UserNew