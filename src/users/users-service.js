import axios from "axios"

export default {
    getUsers() {
        return axios.get('http://localhost:3000/users/')
    },

    getUser(id) {
        return axios.get('http://localhost:3000/users/' + id)
    },

    deleteUser(id) {
        return axios.delete('http://localhost:3000/users/' + id)
    },

    updateUser(user) {
        return axios.put('http://localhost:3000/users/' + user.id, user)
    },

    createUser(data) {
        return axios.post('http://localhost:3000/users/', data)
    }
}